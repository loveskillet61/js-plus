import {getFilm} from './getFilm'
import FilmItem from './FilmItem'
import noposter from '../img/noposter.jpg'

export let textErr = document.querySelector('.search > h3')
textErr.innerHTML = ''

export const dataStorage = JSON.parse(localStorage.getItem('films') || '[]')

export const film = document.querySelector('.films')

export const loadImage = async url => {
    const responce = await fetch(url)
    const film = await responce.json()
    return film
}

export const aboutfilm = document.querySelector('.about-film')

const form = document.querySelector('form')
form.addEventListener('submit', getFilm)

export const app = new Vue({
    el: '#app',
    components: {
        FilmItem
    },
    data: () => {
        return {
            films: [],
            noposter     
        }
    }
})