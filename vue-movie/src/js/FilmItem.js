
import {aboutfilm, loadImage} from './index'
import AboutFilm from './AboutFilm';

export default Vue.component( 'film-item' ,{
    props: ['filmitem', 'noposter'],
    data: () => {
        return {
          toggle: false
        }
    },
    created: function () {
        const films = JSON.parse(localStorage.getItem('films') || "[]")
        const currentMovie = films.find(film => film === this.filmitem.imdbID)
        this.toggle = Boolean(currentMovie)
    },
    methods:{
        openFilm(){
            loadImage(`http://www.omdbapi.com/?i=${this.filmitem.imdbID}&apikey=d5677312`)
            .then (result => {
                const info = new AboutFilm(result)
                aboutfilm.innerHTML = ''
                aboutfilm.innerHTML += info.render()

                document.querySelector('.search').style.display = 'none'
                document.querySelector('.about').style.display = 'block'

                const backButton = document.querySelector('.back')
                backButton.addEventListener('click', e => info.exit(e))
                info.addToStorage()
            })
        },
        insertToStorage(){
            const films = JSON.parse(localStorage.getItem('films') || "[]")
            const currentMovie = films.find(film => film === this.filmitem.imdbID)
            this.toggle = !Boolean(currentMovie)
            console.log(this.toggle)
            if(this.toggle){
              films.push(this.filmitem.imdbID)
              try {
                localStorage.setItem('films', JSON.stringify(films))
              } catch (error) {
                console.log(error);
                alert('Память заполнена.')
              }
              return
            }
            films.map( film => {
                if (film ===  this.filmitem.imdbID){
                    films.splice(films.indexOf(film), 1)
                }
            })
            localStorage.setItem('films', JSON.stringify(films))
          }
    },
    template:  `
    <div class="movie">
        <p class="fav"
            :id="filmitem.imdbID" 
            @click="insertToStorage"
            :class="{ 'fav-on' : this.toggle}">&#10084;</p>
        <img :src="filmitem.Poster !== 'N/A' ? filmitem.Poster : noposter" alt = 'No poster'>
        <p>{{ filmitem.Title }}</p>
        <p>{{ filmitem.Year }}</p>
        <a href="#" :id="filmitem.imdbID" @click.prevent="openFilm">Узнать больше</a>
    </div> `  
})

