import {filmsList} from './filmList'
import {loadImage, textErr} from './index'


export const embeded = url => {
    loadImage(url)
        .then( result => {
            if (result.Response === 'False'){
                throw new Error('Фильм не найден')
            }
            filmsList(result.Search)
        })

        .catch( error => {
            console.log(error)
            film.innerHTML = ''
            textErr.innerHTML = error

        })
}
