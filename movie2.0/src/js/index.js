import {getFilm} from './getFilm'

export let textErr = document.querySelector('.search > h3')
textErr.innerHTML = ''

export const dataStorage = JSON.parse(localStorage.getItem('films') || '[]')

export const film = document.querySelector('.films')

export const loadImage = async url => {
    const responce = await fetch(url)
    const film = await responce.json()
    return film
}

export const aboutfilm = document.querySelector('.about-film')

const form = document.querySelector('form')
form.addEventListener('submit', getFilm)