import {openFilm} from './openFilm'
import Film from './Film'
import {film, dataStorage} from './index'

export const filmsList = result => {
    film.innerHTML = ''

    result.map(movie =>{
        film.innerHTML += new Film(movie).render()
    })

    const favButtons = document.querySelectorAll(`.films .fav`)
    favButtons.forEach( (button, index) => {
        const currentMovie = dataStorage.find(film => film === result[index].imdbID)
        currentMovie ? button.classList.add('fav-on') : ''
        button.addEventListener('click', (e) => addToFavorite(e, button, result[index].imdbID))
    })

    const about = document.querySelectorAll('a')
    about.forEach(link =>{
        let id = link.dataset.id
        link.addEventListener('click', e => openFilm(e, id))
    })
}

export const addToFavorite = (e, button, id) =>{
    e.preventDefault()
    const currentMovie = dataStorage.find(film => film === id)
    let boolCurrentMovie = !Boolean(currentMovie)
    
    button.classList.toggle('fav-on') 
    if(boolCurrentMovie){
        dataStorage.push(button.dataset.id)
    } else {
        const currentFilmIndex = dataStorage.findIndex(film => film === id )
        dataStorage.splice(currentFilmIndex, 1)
    }
    localStorage.setItem('films', JSON.stringify(dataStorage))

}


