import {dataStorage} from './index'
import noposter from '../img/noposter.jpg'

export default class Film {
    constructor(movie){
        this.movie = movie 
        this.favorite = this.isFavorite() 
    }

    render(){
       const {Poster, Title, Year, imdbID } = this.movie 
       return `
        <div class ="movie">
            <p class="fav" data-id="${imdbID}">&#10084;</p>
            <img src = "${ Poster !== 'N/A' ? Poster : noposter}" alt = 'No poster'>
            <p>${Title}</p>
            <p>${Year}</p>
            <a href="#" data-id="${imdbID}">Узнать больше</a>
        </div> `
        
    }

    isFavorite(){
        const movieIsFav = dataStorage.find( movie => movie === this.movie.imdbID )
        return Boolean(movieIsFav)
    }
}
