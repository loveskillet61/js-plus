import {dataStorage} from './index'
import noposter from '../img/noposter.jpg'

export default class AboutFilm{
    constructor(movie){
        this.movie = movie
        this.favorite = this.isFavorite()
    }

    addToStorage(){
        const favButton = document.querySelector('.about .fav')
        this.favorite ? favButton.classList.add('fav-on') : ''
        favButton.onclick = () => {
            this.favorite = !this.favorite
            favButton.classList.toggle('fav-on')
            if(this.favorite){
                dataStorage.push(favButton.dataset.id)
            } else {
                dataStorage.map( film => {
                    if (film === favButton.dataset.id){
                        dataStorage.splice(dataStorage.indexOf(film), 1)
                    }
                })
            }
            localStorage.setItem('films', JSON.stringify(dataStorage))
        }
    }

    isFavorite(){
        const movieIsFav = dataStorage.find( movie => movie === this.movie.imdbID )
        return Boolean(movieIsFav)
    }

    render(){
        const {Poster, Title, Year, Plot, Genre, Actors, Runtime, imdbRating, imdbID } = this.movie
        return  `
        <div class="about">
            <div class="header">
                <button class="back">&#8592;</button>
                <h2>${Title} (${Year})</h2>
                <button class="fav" data-id="${imdbID}">&#10084;</button>
            </div>
            <div class="main">
                <img src = "${ Poster !== 'N/A' ? Poster : noposter}" alt = 'No poster'>
                <div class="info">
                    <p><span>Plot :</span> ${Plot}</p>
                    <p><span>Gerne :</span> ${Genre}</p>
                    <p><span>Runtime :</span> ${Runtime}</p>
                    <p><span>Actors :</span> ${Actors}</p>
                    <p><span>Rating :</span> ${imdbRating}</p>
                </div>
            </div>
        </div>`
    }

    checkFilmIsFavorite(){
        const favButtons = document.querySelectorAll(`.films .fav`)
        favButtons.forEach( (button, index) => {
            const currentMovie = dataStorage.find(film => film === button.dataset.id)
            currentMovie ? button.classList.add('fav-on') : button.classList.remove('fav-on')
        })
    }

    exit(e){
        e.preventDefault()
        this.checkFilmIsFavorite()
        document.querySelector('.search').style.display = 'block'
        document.querySelector('.about').style.display = 'none'
    }

}
