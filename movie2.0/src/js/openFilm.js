import AboutFilm from './AboutFilm'
import {aboutfilm, loadImage} from './index'


export const openFilm = (event, imdbID) =>{
    event.preventDefault()
    loadImage(`http://www.omdbapi.com/?i=${imdbID}&apikey=d5677312`)
    .then (result => {
        console.log(result)
        const info = new AboutFilm(result)
        aboutfilm.innerHTML = ''
        aboutfilm.innerHTML += info.render()

        document.querySelector('.search').style.display = 'none'
        document.querySelector('.about').style.display = 'block'

        const backButton = document.querySelector('.back')
        backButton.addEventListener('click', e => info.exit(e))
        info.addToStorage()
    })
}