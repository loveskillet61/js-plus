textErr = document.querySelector('.search > h3')
textErr.innerHTML = ''
const loadImage = async url => {
    const responce = await fetch(url)
    const film = await responce.json()
    return film
}

const embeded = url => {
    loadImage(url)
        .then( result => {
            if (result.Response === 'False'){
                throw new Error('Фильм не найден')
            }
            filmsList(result.Search)
        })

        .catch( error => {
            console.log(error)
            film.innerHTML = ''
            textErr.innerHTML = error

        })
}
